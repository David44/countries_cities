<style>
    img{
        Max-width: 100%;
        height:auto;
    }
</style>

<?php require APPROOT . '/views/includes/header.php'; ?>

<a href="<?php echo URLROOT; ?>/cities/index/1" class="btn btn-info">Back</a>

<div class="card card-body mt-4">

  <h1><?php echo $data['title']; ?></h1>

  <h3>Upload image</h3>

    <form action="<?php echo URLROOT;?>/cities/create" method="post" enctype="multipart/form-data">

      <div class="form-group">
          <input type="file" accept="image/*"  name="upfile" id="upfile" />
          <br><br>
          <img id="output_image"/>
        </div>
        <input type="hidden" id="countryId" name="countryId" value="<?php echo $_POST['countryId'] ?>">
        City Name <input type="text" name="cityName" ><br>
        <textarea name="cityDescription" rows="4" cols="50" placeholder="Please describe this city a bit"></textarea><br> 
        <input type="submit" class="btn btn-success" value="Post">

    </form>
</div>


