<?php require APPROOT . '/views/includes/header.php'; ?>

<style>
    img{
      max-width:450px;
      max-height:450px;
    }
</style>

<style>
    .button {
     background:none!important;
     color:blue;
     border:none; 
     padding:0!important;
     font: inherit;
     /*border is optional*/
     /*border-bottom:1px solid #444; */
     cursor: pointer;
}
</style>

<a href="<?php echo URLROOT; ?>/cities/index/1" class="btn btn-info">Back</a>

<div class="card card-body mt-4">

  <h3>Search for city</h3>

    <form action="<?php echo URLROOT;?>/cities/search" method="post">

      <div class="form-group">
        <label for="citySearch">Search text: </label>        
        <input type="text" name="citySearch" class="form-control form-control-lg <?php echo (!empty($data['citySearch_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['citySearch']; ?>">
        <span class="invalid-feedback"><?php echo $data['citySearch_error']; ?></span>
      </div>

      <input type="submit" class="btn btn-primary" value="Search">

    </form>

    <br>
    <?php if (empty($data['cities'])) : ?>
      No results.

    <?php else : ?> 

      <div class="card">

      <h4 class="card-title">
        Found results with <span class="text-success"><?php echo $data['citySearch']; ?></span> keyword:
      </h4>
      </div>
      <br>

      <?php foreach ($data['cities'] as $city) : ?>
      
      <div class="card">
        <div class="card-body">

            <img src="<?php echo URLROOT."/public/img/".$city['cityPath']; ?>">
            
            <div class="pull-right">                 
                <form class="pull-right" action="<?php echo URLROOT;?>/cities/delete/<?php echo $city['cityId'];?>" method="post">
                    <input type="submit" value="Delete" class="btn btn-danger">
                </form>
            </div>
            <br> </br>
            <a  href="<?php echo URLROOT; ?>/cities/index"><?php echo $city['cityName'] ?></a>
            <p><?php echo $city['cityDescription'] ?></p>
            
        </div>       
    </div>

      <br>

      <?php endforeach; ?>

    <?php endif; ?>

</div>

<br>



