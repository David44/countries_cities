<style>
    img{
        max-width:450px;
        max-height:450px;
    }
</style>

<style>
    .button {
     background:none!important;
     color:blue;
     border:none; 
     padding:0!important;
     font: inherit;
     /*border is optional*/
     /*border-bottom:1px solid #444; */
     cursor: pointer;
}
</style>

<?php require APPROOT . '/views/includes/header.php'; ?>

<h1><?php echo $data['title']; ?></h1>
<form action="<?php echo URLROOT;?>/cities/search" method="post">

    <div class="form-group row"> 
    <div>
    <input type="text" name="citySearch" class="form-control form-control-lg <?php echo (!empty($data['citySearch_error'])) ? 'is-invalid' : ''; ?>" value="">
    </div>    
    <input type="submit" class="btn btn-primary" value="Search">
    <span class="invalid-feedback"><?php echo $data['citySearch_error']; ?></span>
    </div>

      
</form>
    <a class="btn btn-primary text-white" href="<?php echo URLROOT; ?>/cities/create">Add</a>
    <input type="hidden" id="countryId" name="countryId" value="<?php echo $country['countryId'] ?>">

<br/>
<br/>

    <?php foreach ($data['cities'] as $city) : ?>
    <div class="card">
        <div class="card-body">

            <img src="<?php echo URLROOT."/public/img/".$city['cityPath']; ?>">
            
            <div class="pull-right">                 
                <form class="pull-right" action="<?php echo URLROOT;?>/cities/delete/<?php echo $city['cityId'];?>" method="post">
                    <input type="submit" value="Delete" class="btn btn-danger">
                </form>
            </div>
            <br> </br>
            <a  href="<?php echo URLROOT; ?>/cities/index"><?php echo $city['cityName'] ?></a>
            <p><?php echo $city['cityDescription'] ?></p>
            
        </div>       
    </div>

    <?php endforeach; ?>

    <br>

    <ul class="pagination">
        <?php for($i = 1; $i <= $data['pages']; $i++): ?>
            <li class="page-item <?php if ($data['curentPage'] === $i) { echo 'active';} ?>"><a class="page-link" href="<?php echo URLROOT; ?>/cities/index/<?php echo $i;?>"><?php echo $i; ?></a></li>
        <?php endfor; ?>
    </ul>

