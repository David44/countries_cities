<?php require APPROOT . '/views/includes/header.php'; ?>

<style>
    img{
      max-width:450px;
      max-height:450px;
    }
</style>

<style>
    .button {
     background:none!important;
     color:blue;
     border:none; 
     padding:0!important;
     font: inherit;
     /*border is optional*/
     /*border-bottom:1px solid #444; */
     cursor: pointer;
}
</style>

<a href="<?php echo URLROOT; ?>/countries/index/1" class="btn btn-info">Back</a>

<div class="card card-body mt-4">

  <h3>Search for countries</h3>

    <form action="<?php echo URLROOT;?>/countries/search" method="post">

      <div class="form-group">
        <label for="countrySearch">Search text: </label>        
        <input type="text" name="countrySearch" class="form-control form-control-lg <?php echo (!empty($data['countrySearch_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['countrySearch']; ?>">
        <span class="invalid-feedback"><?php echo $data['countrySearch_error']; ?></span>
      </div>

      <input type="submit" class="btn btn-primary" value="Search">

    </form>

    <br>
    <?php if (empty($data['countries'])) : ?>
      No results.

    <?php else : ?> 

      <div class="card">

      <h4 class="card-title">
        Found results with <span class="text-success"><?php echo $data['countrySearch']; ?></span> keyword:
      </h4>
      </div>
      <br>

      <?php foreach ($data['countries'] as $country) : ?>
      
      <div class="card">
        <div class="card-body">

            <img src="<?php echo URLROOT."/public/img/".$country['countryPath']; ?>">
            
            <div class="pull-right">                 
                <form class="pull-right" action="<?php echo URLROOT;?>/countries/delete/<?php echo $country['countryId'];?>" method="post">
                    <input type="submit" value="Delete" class="btn btn-danger">
                </form>
            </div>
            <br> </br>
            <form class="pull-left" action="<?php echo URLROOT; ?>/cities/index" method="post">
            <input type="hidden" id="countryId" name="countryId" value="<?php echo $country['countryId'] ?>">
            <input class="button" type="submit" value='<?php echo $country['countryName'] ?>'>
            </form>
            <br> </br>
            <p><?php echo $country['CountryDescription'] ?></p> 
            
        </div>       
    </div>

      <br>

      <?php endforeach; ?>

    <?php endif; ?>

</div>

<br>



