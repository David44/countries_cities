<style>
    img{
        max-width:450px;
        max-height:450px;
    }
</style>
    
<style>
    .button {
     background:none!important;
     color:blue;
     border:none; 
     padding:0!important;
     font: inherit;
     /*border is optional*/
     /*border-bottom:1px solid #444; */
     cursor: pointer;
}
</style>

<?php require APPROOT . '/views/includes/header.php'; ?>

<h1><?php echo $data['title']; ?></h1>
<form action="<?php echo URLROOT;?>/countries/search" method="post">

    <div class="form-group row"> 
    <div>
    <input type="text" name="countrySearch" class="form-control form-control-lg <?php echo (!empty($data['countrySearch_error'])) ? 'is-invalid' : ''; ?>" value="">
    </div>    
    <input type="submit" class="btn btn-primary" value="Search">
    <span class="invalid-feedback"><?php echo $data['countrySearch_error']; ?></span>
    </div>

      
</form>
    <a class="btn btn-primary text-white" href="<?php echo URLROOT; ?>/countries/create">Add</a>
<br/>
<br/>

    <?php foreach ($data['country'] as $country) : ?>
    <div class="card">
        <div class="card-body">

            <img src="<?php echo URLROOT."/public/img/".$country['countryPath']; ?>">
            
            <div class="pull-right">                 
                <form class="pull-right" action="<?php echo URLROOT;?>/countries/delete/<?php echo $country['countryId'];?>" method="post">
                    <input type="submit" value="Delete" class="btn btn-danger">
                </form>
            </div>
            <br> </br>
            <form class="pull-left" action="<?php echo URLROOT; ?>/cities/index" method="post">
            <input type="hidden" id="countryId" name="countryId" value="<?php echo $country['countryId'] ?>">
            <input class="button" type="submit" value='<?php echo $country['countryName'] ?>'>
            </form>
            <br> </br>
            <p><?php echo $country['CountryDescription'] ?></p> 
            
        </div>       
    </div>

    <?php endforeach; ?>

    <br>

    <ul class="pagination">
        <?php for($i = 1; $i <= $data['pages']; $i++): ?>
            <li class="page-item <?php if ($data['curentPage'] === $i) { echo ' active';} ?>"><a class="page-link" href="<?php echo URLROOT; ?>/countries/index/<?php echo $i;?>"><?php echo $i; ?></a></li>
        <?php endfor; ?>
    </ul>



