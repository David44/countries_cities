<style>
    img{
        Max-width: 100%;
        height:auto;
    }
</style>

<?php require APPROOT . '/views/includes/header.php'; ?>

<a href="<?php echo URLROOT; ?>/countries/index/1" class="btn btn-info">Back</a>

<div class="card card-body mt-4">

  <h1><?php echo $data['title']; ?></h1>

  <h3>Upload image</h3>

    <form action="<?php echo URLROOT;?>/countries/create" method="post" enctype="multipart/form-data">

      <div class="form-group">
          <input type="file" accept="image/*"  name="upfile" id="upfile" />
          <br><br>
          <img id="output_image"/>
        </div>
        Country Name <input type="text" name="CountryName" ><br>
        <textarea name="CountryDescription" rows="4" cols="50" placeholder="Please describe this country a bit"></textarea><br> 
        <input type="submit" class="btn btn-success" value="Post">

    </form>
</div>


