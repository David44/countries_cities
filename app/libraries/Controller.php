<?php

    // Kontroleris valdytojas, visi kontroleriai ji paveldi

    class Controller {
        // Uzkrauti modeli
        public function model($model) {
            // Require nurodyta model faila
            require_once '../app/models/' . $model . '.php';

            // sukurit model objekta
            return new $model();
        }

        // uzkraunam view faila
        public function view($view, $data = []) {
            // Patikrinti ar yra view failas
            if (file_exists('../app/views/' . $view . '.php')){
                require_once '../app/views/' . $view . '.php';
            }
            else {
                // View neegzistuoja
                die('View ' . $view . ' neegzistuoja!');
            }
        }

    }