<?php

    // App core klase
    // Sukuria URL ir uzkrauna pagrindini kontroleri

    class Core {
        protected $currentController = 'countries';
        protected $currentMethod = 'index';
        protected $params = [];

        public function __construct() {
           
            $url = $this->getUrl();

            // Perziureti kontrolerius ar yra toks kontroleris
            if (file_exists('../app/controllers/' . ucwords($url[0]) . '.php')) {
                // Nustatyti tada kontroleri
                $this->currentController = ucwords($url[0]);
                // Panaikint 0 indeksa
                unset($url[0]);
            }

            // Require kontroleri
            require_once '../app/controllers/'. $this->currentController . '.php';

            // inicijuoti kontroleri
            $this->currentController = new $this->currentController;

            // Patikrinti ar yra kontrolerio metodas
            if (isset($url[1])) {
                // Patikrinti ar metodas egzistuoja kontroleri
                if (method_exists($this->currentController, $url[1])) {
                    $this->currentMethod = $url[1];
                    // Unset URL indeksa
                    unset($url[1]);
                }
            }

            // Gauti likusius URL parametrus
            $this->params = $url ? array_values($url) : [];

            // Kviest callback funckija su masyvu vardais
            call_user_func_array([$this->currentController, $this->currentMethod], [$this->params]);
        }

        public function getUrl() {
            if (isset($_GET['url'])) {
                $url = rtrim($_GET['url'], '/');
                $url = filter_var($url, FILTER_SANITIZE_URL);
                $url = explode('/', $url);
                return $url;
            }
        }
    }

