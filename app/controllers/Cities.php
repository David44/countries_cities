<?php
class Cities extends Controller {

    // Cities konstruktorius, priskiria norimus metodus
    public function __construct() {
        $this->cityModel = $this->model('City');
        $this->countryModel = $this->model('Country');
    }

    // Pradinis miestu puslapis, gaunami visi miestai su puslapiavimu
    public function index($url_params) {
        $page = 1; // default page
        $perPage = 5;
        if (isset($url_params[0])) {
            $page = (int) $url_params[0];
        }
        if (isset($_POST['countryId'])){
            $_SESSION['currentCountry'] = $_POST['countryId'];
        }
        $pages = $this->cityModel->getCitiesRowsTotalPages($perPage, $_SESSION['currentCountry']);

        // Validate pages bounds
        if ($page > $pages){
            $page = (int) $pages;
        }
        else if ($page <= 0) {
            $page = (int) 1;
        }

        $cities = $this->cityModel->getCitiesByPage($page, $perPage, $_SESSION['currentCountry']);
        $country = $this->countryModel->getCountryById($_SESSION['currentCountry']);

        $data = [
            'title' => 'Cities of '.$country['name'],
            'cities' => $cities,
            'curentPage' => $page,
            'pages' => $pages,
            'countryId' => $_SESSION['currentCountry']
        ];

        $this->view('cities/index', $data);
    }
    

    // Sukurti nauja miesta 
    public function create() {
    
        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            // sanitize the city
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'photo' => $_FILES['upfile'],
                'cityDescription' => $_POST['cityDescription'],
                'cityName' => $_POST['cityName'],
                'countryId' => $_SESSION['currentCountry']
            ];

            if($this->cityModel->saveCity($data)){
                redirect('cities/index/1');
            } else {
                echo "Could not upload file.";
            }
        }
        else {
            $data = [
                'title' => ''
            ];

            $this->view('cities/create', $data);
        }

    }

    
    public function search() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){

            // sanitize string
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'citySearch' => trim($_POST['citySearch']),
                'citySearch_error' => '',
                'cities' => []
            ];

            if (empty($data['citySearch'])) {
                $data['citySearch_error'] = 'Please enter a search query';
            }

            // Make sure theare no errorrs
            if (empty($data['citySearch_error'])) {

                // Gauti surastus rezultatus
                $searchResults = $this->cityModel->searchCities($data['citySearch']);

                // Rezultatus priskirti i cities masyva
                $data['cities'] = $searchResults;

                // Pavaizduoti duomenis
                $this->view('cities/search', $data);
                }
            else {
                // Load the view with erros
                $this->view('cities/search', $data);
            }

        }
        else
        {
            $data = [
                'cities' => [],
                'citySearch' => ''
            ];

            $this->view('cities/search', $data);
        }

    }

    

    public function delete($url_params) {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $city_id = $url_params[0];

            //Get the image
            $city = $this->cityModel->getCityById($city_id);
            $path = $city['path'];
           
            if ($this->cityModel->deleteCity($city_id,  $path)) {
                redirect('cities/index/1');
            }
            else {
                redirect('cities/index/1');
            }
        }
        else {
            redirect('cities/index/1');
        }
    }
    

}