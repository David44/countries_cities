<?php
class Countries extends Controller {

    // Countries konstruktorius, priskiria norimus metodus
    public function __construct() {
        $this->countryModel = $this->model('Country');
        $this->cityModel = $this->model('City');
    }


    //Main page, get countries, order by page
    public function index($url_params) {
        $page = 1; // default page
        $perPage = 5;
        //var_dump($url_params) ;
        if (isset($url_params[0])) {
            $page = (int) $url_params[0];
        }
        
        $pages = $this->countryModel->getCountryRowsTotalPages($perPage);

        // Validate pages bounds
        if ($page > $pages){
            $page = (int) $pages;
        }
        else if ($page <= 0) {
            $page = (int) 1;
        }

        $country = $this->countryModel->getCountryByPage($page, $perPage);

        $data = [
            'title' => 'Countries',
            'country' => $country,
            'curentPage' => $page,
            'pages' => $pages
        ];

        $this->view('countries/index', $data);
    }


    // Create new country
    public function create() {
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            // sanitize the country
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'photo' => $_FILES['upfile'],
                'CountryDescription' => $_POST['CountryDescription'],
                'CountryName' => $_POST['CountryName'],
            ];

            if($this->countryModel->saveCountry($data)){
                redirect('countries/index/1');
            } else {
                echo "Could not upload file.";
            }
        }
        else {
            $data = [
                'title' => ''
            ];

            $this->view('countries/create', $data);
        }
        
    }

    
    //Delete an image
    public function delete($url_params) {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $country_id = $url_params[0];

            //Get the image
            $country = $this->countryModel->getCountryById($country_id);
            $path = $country['path'];
           // delete associated cities
            $this->cityModel->deleteCities($country_id);
            if ($this->countryModel->deleteCountry($country_id,  $path)) {
               
                redirect('countries/index/1');
            }
            else {
                redirect('countries/index/1');
            }
        }
        else {
            redirect('countries/index/1');
        }
    }


    public function search() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){

            // sanitize string
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'countrySearch' => trim($_POST['countrySearch']),
                'countrySearch_error' => '',
                'countries' => []
            ];

            if (empty($data['countrySearch'])) {
                $data['countrySearch_error'] = 'Please enter a search query';
            }

            // Make sure there are no errorrs
            if (empty($data['countrySearch_error'])) {

                // Gauti surastus rezultatus
                $searchResults = $this->countryModel->searchCountries($data['countrySearch']);

                // Rezultatus priskirti i countries masyva
                $data['countries'] = $searchResults;

                // Pavaizduoti duomenis
                $this->view('countries/search', $data);
                }
            else {
                // Load the view with erros
                $this->view('countries/search', $data);
            }

        }
        else
        {
            $data = [
                'countries' => [],
                'countrySearch' => ''
            ];

            $this->view('countries/search', $data);
        }
    }
}