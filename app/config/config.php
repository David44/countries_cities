<?php
    // duombazes parametrai
    define('DB_HOST', 'localhost');
    define('DB_USER', 'root');
    define('DB_PASS', '');
    define('DB_NAME', 'countries_cities');

    // production img upload
    define('IMG_UPLOAD_ROOT', $_SERVER['DOCUMENT_ROOT'].'/countries_cities/public/img/%s.%s');

    define('UNROOT', "/var/www/countries_cities/public/img/");

    // APP pradzia (root)
    define('APPROOT', dirname(dirname(__FILE__)));

    // URL (koks yra svetaines domenas(localhost);
    define('URLROOT', 'http://localhost/countries_cities');

    // Svetaines vardas
    define('SITENAME', 'Countries And Cities');
