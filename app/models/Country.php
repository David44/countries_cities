<?php
class Country {
    private $db; // duomenu baze

    // Sukuriam nauja PDO prisjungima prie duomenu bazes
    public function __construct() {
        $this->db = new Database;
    }

    // Gauti salis pagal nurodyta puslapi($page) ir kiek tame puslapyje yra saliu ($perPage)
    public function getCountryByPage($page, $perPage) {
        $startPosition = ($page > 1) ? ($page * $perPage) - $perPage : 0;

        $this->db->query("SELECT *, 
                          country.id as countryId,
                          country.name as countryName,
                          country.description as CountryDescription,
                          country.path as countryPath
                          FROM country
                          ORDER BY country.created_at DESC
                          LIMIT $startPosition, $perPage");

        $results = $this->db->getResults();

        return $results;
    }

    // Suskaiciuojam kiek is viso yra saliu
    public function getCountryRowsTotalPages($perPage) {
        $this->db->query("SELECT *
                          FROM country");

        $results = $this->db->getResults();

        $totalRows = $this->db->getRowCount();
        $pages = ceil($totalRows / $perPage);

        return $pages;
    }

    // Gauti sali pagal jos id
    public function getCountryById($id) {
        $this->db->query('SELECT * FROM country WHERE id = :id');
        $this->db->bind(':id', $id);
  
        $row = $this->db->singleResult();
  
        return $row;
    }

    // Pridedam sali i duomenu baze
    public function saveCountry($data){
        header('Content-Type: text/plain; charset=utf-8');

        try {

            // Undefined | Multiple Files | $_FILES Corruption Attack
            // If this request falls under any of them, treat it invalid.
            if (
                !isset($data['photo']['error']) ||
                is_array($data['photo']['error'])
            ) {
                throw new RuntimeException('Invalid parameters.');
            }

            // Check $_FILES['upfile']['error'] value.
            switch ($data['photo']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            // You should also check filesize here.
            if ($data['photo']['size'] > 1000000) {
                throw new RuntimeException('Exceeded filesize limit.');
            }

            // DO NOT TRUST $_FILES['upfile']['mime'] VALUE !!
            // Check MIME Type by yourself.
            $finfo = new finfo(FILEINFO_MIME_TYPE);
            if (false === $ext = array_search(
                    $finfo->file($data['photo']['tmp_name']),
                    array(
                        'jpg' => 'image/jpeg',
                        'png' => 'image/png',
                        'gif' => 'image/gif',
                    ),
                    true
                )) {
                throw new RuntimeException('Invalid file format.');
            }

            $count = $this->db->query('SELECT count(*) AS c FROM country');
            $size = $this->db->singleResult();

            echo $size['c'];

            if($size['c'] > 15000) {
                throw new RuntimeException('Server overloaded :(');
            }

            $path = sprintf(IMG_UPLOAD_ROOT,
                sha1_file($data['photo']['tmp_name']),
                $ext
            );

            $path2 = sha1_file($data['photo']['tmp_name']).".".$ext;

            // You should name it uniquely.
            // DO NOT USE $_FILES['upfile']['name'] WITHOUT ANY VALIDATION !!
            // On this example, obtain safe unique name from its binary data.
            if (!move_uploaded_file(
                $data['photo']['tmp_name'],
                $path
            )) {
                echo $_SERVER['DOCUMENT_ROOT'];
                throw new RuntimeException('Failed to move uploaded file.');
            }
            echo 'File is uploaded successfully.';

            $this->db->query("INSERT INTO country (path, name, description) VALUES (:path, :name, :description)");

            $this->db->bind(":path", $path2);
            $this->db->bind(":name", $data['CountryName']);
            $this->db->bind(":description", $data['CountryDescription']);

            // execute query
            if ($this->db->execute()) {
                return true;
            }
            else {
                return false;
            }

        } catch (RuntimeException $e) {
            echo $e->getMessage();
        }
    }

    // Gauti salis, kurios atitinka duota raktazodi
    public function searchCountries($keyword) {

        $likeKeyword = '%' . $keyword . '%';

        $this->db->query("SELECT *,
                        country.id as countryId,
                        country.name as countryName,
                        country.description as CountryDescription,
                        country.path as countryPath 
                        FROM country
                        WHERE country.name LIKE :likeKeyword 
                        OR country.description LIKE :likeKeyword
                        ORDER BY country.created_at DESC"); 
        
        $this->db->bind(':likeKeyword', $likeKeyword);

        $results = $this->db->getResults();

        return $results;
    }

    // Istrinti sali
    public function deleteCountry($Country_id) {
        $this->db->query('DELETE FROM country WHERE id = :id');
  
        // bind values
        $this->db->bind(':id', $Country_id);
  
        // execute query
        if ($this->db->execute()) {
          return true;
        }
        else {
          return false;
        }
    }

}